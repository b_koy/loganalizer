package com.log.finder.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import com.log.finder.main.Constant;
import com.log.finder.main.FolderSearcher;

public class Window extends JFrame {
    private static final long serialVersionUID = 1L;

    private static final int HEIGHT = 23;
    private static final Integer[] counts = { 0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 };
    private static final Color LABEL_COLOR = Color.WHITE;
    private static final int Y = 40;

    private Thread thread = null;
    private FolderSearcher folderSearcher = new FolderSearcher();

    JButton searchButton = new JButton("Search");
    JButton stopButton = new JButton("Stop");
    JButton clsButton = new JButton("CLS");
    JButton browseButton = new JButton("Browse");
    JButton browseSaveButton = new JButton("Browse");
    JPanel panel = new JPanel();
    JLabel name = new JLabel("Log Analyzer v.2");
    JLabel pharseLabel = new JLabel("Phrase for analyzing:");
    JTextArea phrase = new JTextArea("text");
    JCheckBox regexCheckBox = new JCheckBox("Use regex");
    JCheckBox caseSensetiveBox = new JCheckBox("Case sensetive");
    JLabel pathLabel = new JLabel("Path to folder with logs:");
    JTextArea path = new JTextArea(Constant.DEFAULT_SAVE_FOLDER);
    JLabel pathSaveLabel = new JLabel("Path for save results:");
    JTextArea pathSave = new JTextArea(Constant.DEFAULT_SAVE_FOLDER
            + Constant.RESULT_FOLDER);
    JLabel beforeLabel = new JLabel("Lines quantity before result line:");
    JComboBox<Integer> before = new JComboBox<Integer>(counts);
    JLabel afterLabel = new JLabel("Lines quantity after result line:");
    JComboBox<Integer> after = new JComboBox<Integer>(counts);
    JTextArea status = new JTextArea();
    JScrollPane jScrollPane = new JScrollPane(status);
    Border border = BorderFactory.createLineBorder(Color.BLACK);
    Border emptyBorder = BorderFactory.createEmptyBorder(4, 4, 4, 6);
    JLabel author = new JLabel("Author: bkolesnyk85@gmail.com");
    JLabel consolName = new JLabel("Console:");

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Window();
            }
        });
    }

    public Window() {
        super("LogAnalyzer_v.2");
        init();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 590 + Y);
        setResizable(false);
        setContentPane(panel);
        setVisible(true);
    }

    private void init() {
        panel.setOpaque(true);
        panel.setBackground(Color.DARK_GRAY);
        panel.setLayout(null);

        // logo
        name.setSize(200, HEIGHT);
        name.setLocation(600, 20);
        name.setForeground(LABEL_COLOR);
        name.setFont(new Font(author.getName(), Font.BOLD, 20));

        // search phrase
        pharseLabel.setSize(200, HEIGHT);
        pharseLabel.setLocation(20, 20 + Y);
        pharseLabel.setForeground(LABEL_COLOR);

        phrase.setSize(300, HEIGHT);
        phrase.setLocation(160, 20 + Y);
        phrase.setBackground(Color.WHITE);
        phrase.setBorder(border);
        phrase.setBorder(emptyBorder);

        regexCheckBox.setSize(100, 20);
        regexCheckBox.setLocation(465, 20 + Y);
        regexCheckBox.setBackground(Color.DARK_GRAY);
        regexCheckBox.setForeground(LABEL_COLOR);
        
        caseSensetiveBox.setSize(120, 20);
        caseSensetiveBox.setLocation(580, 20 + Y);
        caseSensetiveBox.setBackground(Color.DARK_GRAY);
        caseSensetiveBox.setForeground(LABEL_COLOR);
               
        // search folder path
        pathLabel.setSize(200, HEIGHT);
        pathLabel.setLocation(20, 50 + Y);
        pathLabel.setForeground(LABEL_COLOR);

        path.setSize(520, HEIGHT);
        path.setLocation(160, 50 + Y);
        path.setBackground(Color.WHITE);
        path.setBorder(border);
        path.setBorder(emptyBorder);

        browseButton.setLocation(700, 50 + Y);
        browseButton.setSize(80, HEIGHT);
        browseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                fileopen.setCurrentDirectory(new File(path.getText()));
                fileopen.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int ret = fileopen.showDialog(null, "Choose folder");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    path.setText(file.getPath());
                    pathSave.setText(file.getPath() + Constant.RESULT_FOLDER);
                }
            }
        });

        // save folder path
        pathSaveLabel.setSize(200, HEIGHT);
        pathSaveLabel.setLocation(20, 80 + Y);
        pathSaveLabel.setForeground(LABEL_COLOR);

        pathSave.setSize(520, HEIGHT);
        pathSave.setLocation(160, 80 + Y);
        pathSave.setBackground(Color.WHITE);
        pathSave.setBorder(border);
        pathSave.setBorder(emptyBorder);

        browseSaveButton.setLocation(700, 80 + Y);
        browseSaveButton.setSize(80, HEIGHT);
        browseSaveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                fileopen.setCurrentDirectory(new File(pathSave.getText()));
                fileopen.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int ret = fileopen.showDialog(null, "Choose folder");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    pathSave.setText(file.getPath());
                }
            }
        });

        // line counts
        beforeLabel.setSize(300, HEIGHT);
        beforeLabel.setLocation(20, 110 + Y);
        beforeLabel.setForeground(LABEL_COLOR);

        before.setSize(45, 20);
        before.setLocation(210, 110 + Y);
        before.setBackground(Color.WHITE);
        before.setBorder(border);

        afterLabel.setSize(300, HEIGHT);
        afterLabel.setLocation(20, 140 + Y);
        afterLabel.setForeground(LABEL_COLOR);

        after.setSize(45, 20);
        after.setLocation(210, 140 + Y);
        after.setBackground(Color.WHITE);
        after.setBorder(border);

        // cls button
        clsButton.setLocation(500, 180 + Y);
        clsButton.setSize(80, 30);
        clsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                status.setText("");
            }
        });
        
        // stop button
        stopButton.setLocation(600, 180 + Y);
        stopButton.setSize(80, 30);
        stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("\n[Search process is stopping]\n");
                folderSearcher.interrupt();
            }
        });
        stopButton.setEnabled(false);

        // search button
        searchButton.setLocation(700, 180 + Y);
        searchButton.setSize(80, 30);
        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        searchButton.setEnabled(false);
                        stopButton.setEnabled(true);
                        clsButton.setEnabled(false);
                        try {
                            folderSearcher.searchInFolder(path.getText(),
                                    pathSave.getText(), 
                                    phrase.getText(),
                                    (Integer) before.getSelectedItem(),
                                    (Integer) after.getSelectedItem(),
                                    regexCheckBox.isSelected(),
                                    caseSensetiveBox.isSelected());
                        } finally {
                            searchButton.setEnabled(true);
                            stopButton.setEnabled(false);
                            clsButton.setEnabled(true);
                        }
                    }
                });
                thread.start();
            }
        });

        // console
        consolName.setLocation(80, 200 + Y);
        consolName.setSize(300, 40);
        consolName.setForeground(Color.WHITE);

        status.setLocation(80, 220 + Y);
        status.setSize(300, 40);
        status.setBackground(Color.BLACK);
        status.setForeground(Color.WHITE);

        jScrollPane.setSize(700, 300);
        jScrollPane.setLocation(80, 230 + Y);

        // Redirect default System.out and System.err to CustomOutputStream
        PrintStream printStream = new PrintStream(
                new CustomOutputStream(status));
        System.setOut(printStream);
        System.setErr(printStream);

        // author
        author.setLocation(600, 535 + Y);
        author.setSize(200, HEIGHT);
        author.setForeground(LABEL_COLOR);
        author.setFont(new Font(author.getName(), Font.ITALIC, 10));

        // panel init
        panel.add(name);
        panel.add(pharseLabel);
        panel.add(phrase);
        panel.add(regexCheckBox);
        panel.add(caseSensetiveBox);
        panel.add(pathLabel);
        panel.add(path);
        panel.add(browseButton);
        panel.add(pathSaveLabel);
        panel.add(pathSave);
        panel.add(browseSaveButton);
        panel.add(beforeLabel);
        panel.add(before);
        panel.add(afterLabel);
        panel.add(after);
        panel.add(clsButton);
        panel.add(stopButton);
        panel.add(searchButton);
        panel.add(consolName);
        panel.add(jScrollPane);
        panel.add(author);
    }
}
