/**
 * 
 */
package com.log.finder.main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * @author Bohdan_Kolesnyk
 *
 */
public class LogFileSearcher extends FileSearcher {

    public LogFileSearcher(File file, String storePath, int beforeCount,
            int afterCount) {
        super(file, storePath, beforeCount, afterCount);
    }

    @Override
    protected List<String> getStringListFromFile() throws IOException {
        Path filePath = file.toPath();
        Charset charset = Charset.defaultCharset();
        return Files.readAllLines(filePath, charset);
    }
}
