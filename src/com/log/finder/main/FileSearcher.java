/**
 * 
 */
package com.log.finder.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bohdan_Kolesnyk
 *
 */
abstract public class FileSearcher {
	protected File file;
	protected String storePath;
	protected int beforeCount;
	protected int afterCount;

	public FileSearcher(File file, String storePath, int beforeCount, int afterCount) {
		super();
		this.file = file;
		this.storePath = storePath;
		this.beforeCount = beforeCount;
		this.afterCount = afterCount;
	}

	public void search(String searchPhrase, boolean isRegex, boolean isCaseSensetive) {
		int foundResults = 0;
		String fileName = file.getName();
		String filePath = file.getPath();
		Utils.print("Open file - " + filePath);
		try (FileWriter fileWriter = new FileWriter(storePath
				+ Constant.FILE_NAME_PREFIX + fileName + ".txt");) {
			List<String> stringList = getStringListFromFile();
			Utils.print("Create file - " + storePath
					+ Constant.FILE_NAME_PREFIX + filePath + ".txt ");
			for (int i = 0; i < stringList.size(); i++) {
				if ((isRegex && containsRegex(stringList.get(i), searchPhrase))
						|| (!isRegex && containsText(stringList.get(i), searchPhrase, isCaseSensetive))) {
					// System.out.println("[Found on line - " + i + " ]");
					fileWriter.write("===== Line number in log - " + i
							+ " =====");
					fileWriter.write("\n");
					for (String line : getList(stringList, i, beforeCount,
							afterCount)) {
						if (containsText(line,searchPhrase,isCaseSensetive)) {
							fileWriter.write(">>> ");
							foundResults++;
						} else {
							fileWriter.write("    ");
						}
						fileWriter.write(line);
						fileWriter.write("\n");
					}
					i += afterCount;
					fileWriter.write(Constant.DELIMETER);
					fileWriter.write("\n");
					fileWriter.write("\n");
				}
			}
			Utils.print("Found - " + foundResults + " result(s)");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Utils.print("Close file - " + filePath + "\n");
	}

	abstract protected List<String> getStringListFromFile() throws IOException;

	private boolean containsRegex(String line, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(line);
		if (m.find()) {
			return true;
		}
		return false;
	}
	
	private boolean containsText(String line, String text, boolean isCaseSensetive){
		line = isCaseSensetive ? line : line.toLowerCase();
		text = isCaseSensetive ? text : text.toLowerCase(); 
		return line.contains(text);
	}

	/**
	 * Get list searched results
	 * 
	 * @param list
	 * @param index
	 * @param lineCountBefore
	 * @param lineCountAfter
	 * @return
	 */
	private List<String> getList(List<String> list, int index,
			int lineCountBefore, int lineCountAfter) {
		List<String> resultList = new ArrayList<String>();
		if (index < lineCountBefore) {
			lineCountBefore = 0;
		} else {
			lineCountBefore = index - lineCountBefore;
		}
		lineCountAfter += index;
		lineCountAfter++;
		if (lineCountAfter >= list.size()) {
			lineCountAfter = list.size() - 1;
		}
		for (int i = lineCountBefore; i < lineCountAfter; i++) {
			resultList.add(list.get(i));
		}
		return resultList;
	}
}
