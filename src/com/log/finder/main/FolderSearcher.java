package com.log.finder.main;

import java.io.File;
import java.io.IOException;

/**
 * Class which stores search logic
 * 
 * @author Bohdan_Kolesnyk
 *
 */
public class FolderSearcher {

	private boolean isInterrupted = false;

	public FolderSearcher() {
	}

	/**
	 * Search by phrase in folder
	 * 
	 * @param path
	 *            - path to folder
	 * @param search
	 *            - search phrase
	 * @param beforeCount
	 *            - count of lines before result
	 * @param afteCount
	 *            - count of lines after result
	 * @throws IOException
	 */
	public void searchInFolder(String path, String savePath, String search,
			int beforeCount, int afterCount, boolean isRegex, boolean isCaseSensetive) {
		Utils.print("Searching STARTED in folder: " + path + "\n");
		File folder = new File(path);
		new File(savePath).mkdir();
		for (File file : folder.listFiles()) {
			if (isInterrupted) {
				Utils.print("Search Stopped!");
				isInterrupted = false;
				break;
			}
			if (!file.isDirectory()) {
				searchInFile(file, savePath, search, beforeCount, afterCount,
						isRegex, isCaseSensetive);
			} else if(file.getName().contains("search_results")){
			    continue;
			} else {
				//search in files in folder
				for (File innerFile : file.listFiles()) {
					if (isInterrupted) {
						Utils.print("Search Stopped!");
						isInterrupted = false;
						break;
					}
					String innerResultFolder = savePath + "\\" + file.getName();
					new File(innerResultFolder).mkdir();
					searchInFile(innerFile, innerResultFolder , search, beforeCount, afterCount,
							isRegex, isCaseSensetive);
				}
			}
		}
		Utils.print("Searching FINISHED");
		Utils.print("All results was stored at: " + path
				+ Constant.RESULT_FOLDER + "\n");
	}

	public void searchInFile(File file, String storePath,
			String searchPhrase, int beforeCount, int afterCount,
			boolean isRegex, boolean isCaseSensetive) {
		String fileName = file.getName().toLowerCase();
		if (fileName.contains(".gz")) {
			new GzipFileSearcher(file, storePath, beforeCount, afterCount)
					.search(searchPhrase, isRegex, isCaseSensetive);
		} else if (fileName.contains(".zip")) {
			new ZipFileSearcher(file, storePath, beforeCount, afterCount)
					.search(searchPhrase, isRegex, isCaseSensetive);
		} else if (fileName.contains(".log")) {
			new LogFileSearcher(file, storePath, beforeCount, afterCount)
					.search(searchPhrase, isRegex, isCaseSensetive);
		} else {
			Utils.print("File " + fileName
					+ " has not supported format\n");
		}
	}

	/**
	 * Interrupt search
	 */
	public void interrupt() {
		isInterrupted = true;
	}

}
