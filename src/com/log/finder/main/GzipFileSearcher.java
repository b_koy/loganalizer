/**
 * 
 */
package com.log.finder.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * @author Bohdan_Kolesnyk
 *
 */
public class GzipFileSearcher extends FileSearcher {

    public GzipFileSearcher(File file, String storePath, int beforeCount,
            int afterCount) {
        super(file, storePath, beforeCount, afterCount);
    }

    @Override
    protected List<String> getStringListFromFile() {
        List<String> resultList = new ArrayList<String>();
		try (BufferedReader scanner = new BufferedReader(new InputStreamReader(new GZIPInputStream(
				new FileInputStream(file))))) {
			String line;
			while ((line = scanner.readLine()) != null) {
				resultList.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultList;
	}

}
