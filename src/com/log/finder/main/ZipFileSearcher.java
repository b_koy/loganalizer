package com.log.finder.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ZipFileSearcher extends FileSearcher {

    public ZipFileSearcher(File file, String storePath, int beforeCount,
            int afterCount) {
        super(file, storePath, beforeCount, afterCount);
    }

    @Override
    protected List<String> getStringListFromFile() throws IOException {
        List<String> resultList = new ArrayList<String>();
        ZipFile zip = null;
        ZipInputStream zipInputStream = null;
        Scanner scanner = null;
        try {
            zip = new ZipFile(file);
            zipInputStream = new ZipInputStream(new FileInputStream(file));
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            scanner = new Scanner(zip.getInputStream(zipEntry));
            while (scanner.hasNext()) {
                resultList.add(scanner.nextLine());
            }
        } finally {
            scanner.close();
            zipInputStream.close();
            zip.close();
        }
        return resultList;
    }
}
